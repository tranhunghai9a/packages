import 'package:flutter/material.dart';
import 'package:string_util/string_util.dart';
import 'package:tiengviet/tiengviet.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Packages #1',
      home: PackageScreen(),
    );
  }
}

class PackageScreen extends StatefulWidget {
  const PackageScreen({Key? key}) : super(key: key);

  @override
  _PackageScreenState createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  final TextEditingController yourText = TextEditingController();
  String result = 'number here ';
  String result2 = ' non-unicode string here';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Package String App'),
      ),
      body: Column(
        children: [
          AppTextField(yourText, 'Input string here'),
          const SizedBox(height: 18),
          ElevatedButton(
            child: const Text('Result'),
            onPressed: (){
              String text = yourText.text;
              String res = countText(text);
              String res2 = TiengViet.parse(text);
              setState(() {
                result = res;
                result2 = res2;
              });
            },
          ),
          const SizedBox(height: 18),
          Text('The number of words is: '+ result + ' non-unicode: ' + result2, style: TextStyle(color: Colors.red, fontSize: 24),),
        ],
      ),
    );
  }
}

class AppTextField extends StatelessWidget {
  final TextEditingController controller;
  final String label;

  AppTextField(this.controller, this.label);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(24),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(hintText: label),
      ),
    );
  }
}

